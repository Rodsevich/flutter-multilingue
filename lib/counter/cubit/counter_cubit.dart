// Copyright (c) 2022, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:bloc/bloc.dart';
import 'package:i18n/counter/cubit/counter_state.dart';

class CounterCubit extends Cubit<CubitCounterState> {
  CounterCubit() : super(const CubitCounterState());

  void increment() => emit(state.copyWith(count: state.count + 1));
  void decrement() => emit(state.copyWith(count: state.count - 1));
  void changeSex(Sexo sexo) => emit(state.copyWith(sexo: sexo));
}
