import 'package:equatable/equatable.dart';

enum Sexo { masculino, femenino, other }

class CubitCounterState extends Equatable {
  const CubitCounterState({
    this.sexo = Sexo.masculino,
    this.count = 0,
  });

  final Sexo sexo;
  final int count;

  @override
  List<Object> get props => [sexo, count];

  CubitCounterState copyWith({
    Sexo? sexo,
    int? count,
  }) {
    return CubitCounterState(
      sexo: sexo ?? this.sexo,
      count: count ?? this.count,
    );
  }
}
