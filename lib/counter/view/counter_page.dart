// Copyright (c) 2022, Very Good Ventures
// https://verygood.ventures
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

import 'package:arb_utils/arb_utils_flutter.dart';
import 'package:arb_utils/state_managers/l10n_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:i18n/counter/counter.dart';
import 'package:i18n/counter/cubit/counter_state.dart';
import 'package:i18n/l10n/l10n.dart';

class CounterPage extends StatelessWidget {
  const CounterPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => CounterCubit(),
      child: const CounterView(),
    );
  }
}

class CounterView extends StatelessWidget {
  const CounterView({super.key});

  @override
  Widget build(BuildContext context) {
    final l10n = context.l10n;
    final estado = context.select((CounterCubit cubit) => cubit.state);
    final count = estado.count;
    final sexo = estado.sexo;
    return Scaffold(
      appBar: AppBar(
        title: Text(l10n.counterAppBarTitle),
        actions: [
          LanguageSelectorDropdown(
            supportedLocales: AppLocalizations.supportedLocales,
            provider: context.read<ProviderL10n>(),
          ),
          const SizedBox(width: 10),
        ],
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(l10n.counterViewSexLabel),
                  _OptionButton(
                      sexo: Sexo.masculino,
                      url:
                          'https://cdn.pixabay.com/photo/2012/05/04/10/51/man-47185_1280.png',
                      color: Colors.blue),
                  _OptionButton(
                      sexo: Sexo.femenino,
                      url:
                          'https://www.clipartmax.com/png/small/59-594793_woman-silhouette-pink-clip-art-your-girlfriend-me-pole.png',
                      color: Colors.pink),
                  _OptionButton(
                      sexo: Sexo.other,
                      url:
                          'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRhZaHAqKWZWjyGzRasENi8IttGdhUWHLVHvlUPCaENjS_8n3j9zkwDKVoMpqEzeEzc-fg&usqp=CAU',
                      color: Colors.green),
                ]
                    .map((w) => [w, const SizedBox(width: 10)])
                    .expand((e) => e)
                    .toList()
                  ..removeLast()),
            Text(l10n.counterViewGreeting(sexo.name),
                style: Theme.of(context).textTheme.headline4),
            const CounterText(),
            Text(l10n.counterViewCurrentCounterNumber(count),
                style: Theme.of(context).textTheme.headline5),
            const SizedBox(),
          ],
        ),
      ),
      floatingActionButton: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          FloatingActionButton(
            onPressed: () => context.read<CounterCubit>().increment(),
            child: const Icon(Icons.add),
          ),
          const SizedBox(height: 8),
          FloatingActionButton(
            onPressed: () => context.read<CounterCubit>().decrement(),
            child: const Icon(Icons.remove),
          ),
        ],
      ),
    );
  }
}

class _OptionButton extends StatelessWidget {
  const _OptionButton({
    Key? key,
    required this.url,
    required this.color,
    required this.sexo,
  }) : super(key: key);
  final String url;
  final Color color;
  final Sexo sexo;

  @override
  Widget build(BuildContext context) {
    final estado = context.select((CounterCubit cubit) => cubit.state);
    final chosen = estado.sexo == sexo;
    return DecoratedBox(
      decoration: BoxDecoration(
        border: Border.all(color: chosen ? color : Colors.transparent),
        borderRadius: BorderRadius.circular(10),
      ),
      child: IconButton(
        onPressed: () => context.read<CounterCubit>().changeSex(sexo),
        icon: Image.network(url),
      ),
    );
  }
}

class CounterText extends StatelessWidget {
  const CounterText({super.key});

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final count = context.select((CounterCubit cubit) => cubit.state.count);
    return Text('$count', style: theme.textTheme.headline1);
  }
}
